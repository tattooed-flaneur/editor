from flask import Flask
from flask import redirect, render_template
from flask import request, url_for

app = Flask(__name__)

import collections
import json

def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        try:
            items.extend(flatten(v, new_key, sep=sep).items())
        except:
            items.append((new_key, v))
    return dict(items)

event = json.loads('{"@context": "http://schema.org/", "location": {"geo": {"@type": "GeoCoordinates", "longitude": 6.9878306, "latitude": 49.2423439}, "name": "Stiftung Demokratie Saarland", "address": {"addressLocality": "Saarbrücken, Germany", "@type": "PostalAddress", "streetAddress": "Europaallee 18", "postalCode": "66113"}, "url": "https://www.facebook.com/161990010584761", "@type": "Place"}, "url": "https://www.facebook.com/236358937283538", "@type": "Event", "enddate": "2019-01-11T20:00:00+0100", "name": "Finissage: Menschenschatten Lombre des hommes", "startdate": "2019-01-11T18:00:00+0100", "description": "FINISSAGE mit Kunstaktion Raku-sho klingende ZeichenIn ihrem kontemplativen Duktus stehen die Tuschezeichnungen ganz in der Tradition der Zen-Malerei, werden jedoch durch geometrisch-strenge Strichinterventionen herausgefordert. Auf diese Weise verwandeln sie Stillstand in Bewegung, Schwere in Leichtigkeit und Geschlossenheit in Öffnung, sodass die kraftvollen Pinselschwünge und die filigranen Striche in einen spannenden Dialog treten. Während uns die skulpturalen Arbeiten Schmerzgrenzen regelrecht erspüren lassen, werden wir von den Tuschearbeiten behutsam ins Zentrum des Unrechts geleitet.In der Kunstaktion Raku-sho klingende Zeichen werden der Künstler Seiji Kimoto und der Bassist Rudolf Schaaf mit Pinsel und Kontrabass interagieren, improvisieren, sich im Visuellen wie im Akustischen begegnen, trennen und wieder vereinen. Die BesucherInnen werden eingebunden in ein konzentriertes Wechselspiel aus Klang und Zeichen.Mit dieser Veranstaltung endet die Ausstellung Menschenschatten Lombre des hommes, die wir in Kooperation mit dem Parc Explor Wendel gezeigt haben.in Kooperation mit Parc Explor Wendel Petite-Rosselle"}')


def get_event(id=None):
    return flatten(event)

def get_ld():
   return json.dumps(event)


@app.route('/')
def hello():
    return "Hello World!"


@app.route('/edit/')
@app.route('/edit/<id>', methods=['POST', 'GET'])
def edit(id=None):
    thing = get_event()
    ld = get_ld()
    return render_template('edit.html', thing=thing, ld=ld)


@app.route('/save/', methods=['POST'])
def save():
    if request.method == 'POST':
        schema_type = request.form['@type']
        print(schema_type)
        print(request.form['name'])
        event['name'] = request.form['name']
        ld = json.dumps(event)
    return redirect(url_for('edit'))

if __name__ == '__main__':
    app.run()
